import React, {Component} from 'react';
import {
    Container,
    Row,
    Col,
    FormGroup,
    Input,
    Button,
} from 'reactstrap';
import classnames from 'classnames'
import ReactLoading from "react-loading";
import get from 'lodash/get';
import axios from 'axios';
import AverageGauge from '../AverageGauge/AverageGauge';
import PercentageGauge from '../PercentageGauge/PercentageGauge';
import { description } from '../data';

import styles from './App.module.css'

class App extends Component {

    state = {
        keyword: '',
        subkey: '',
        isLoading: false,
        data: null,
        errors: {}
    };

    handleOnKeywordChange = e => {
        const keyword = e.target.value;
        this.setState({
            keyword,
        });
    };

    handleOnSubKeyChange = e => {
        const subkey = e.target.value;
        this.setState({
            subkey,
        });
    };

    handleSearchKeyUp = async e => {
        e.preventDefault();
        if (e.keyCode === 13) {
            await this.handleOnSubmit();
        }
    };

    handleOnSubmit = async () => {
        if (this.state.keyword) {
            this.setState({
                data: null,
                isLoading: true,
                errors: {}
            });
            try {
                const result = await axios.get(`http://localhost:8000/api/search?keyword=${this.state.keyword}&subkey=${this.state.subkey}`);
                this.setState({
                    data: result.data ,
                    isLoading: false,
                }, () => this.forceUpdate())
            } catch (err) {
                console.log(err);
            }
        } else {
            this.setState({
                errors: {
                    keyword: 'Keyword is required',
                }
            })
        }
    };

    renderSentimentPercentages() {
        const total = get(this.state, 'data.count', 0);
        if (total) {
            const negativeTotal = get(this.state, 'data.negative', []).length;
            const negative = negativeTotal / total;
            const neutralTotal = get(this.state, 'data.neutral', []).length;
            const neutral = neutralTotal / total;
            const goodTotal = get(this.state, 'data.good', []).length;
            const good = goodTotal / total;

            return (
                <Row className="mt-5">
                    <Col><PercentageGauge value={negative} color={'#eb0100'} total={negativeTotal} label="Negative" /></Col>
                    <Col><PercentageGauge value={neutral} color={'#d6b606'} total={neutralTotal} label="Neutral" /></Col>
                    <Col><PercentageGauge value={good} color={'#0bd500'} total={goodTotal} label="Good" /></Col>
                </Row>
            )
        }
    }

    render() {
        return (
            <Container className={classnames(styles.app, 'p-4')}>
                <Row>
                    <Col className="text-center mb-4">
                        <img className={styles.logo} src="/images/logo.png" alt=""/><h1 className={classnames(styles.header, 'my-3')}>Sentimentool</h1>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={{ size: 6, offset: 3 }} className="pb-2">
                        <FormGroup>
                            <Input
                                onChange={this.handleOnKeywordChange}
                                onKeyUp={this.handleSearchKeyUp}
                                placeholder="Keyword"
                            />
                            <div className="error">{get(this.state, 'errors.keyword')}</div>
                        </FormGroup>
                        <FormGroup>
                            <Input
                                onChange={this.handleOnSubKeyChange}
                                onKeyUp={this.handleSearchKeyUp}
                                placeholder="Sub-keyword"
                            />
                        </FormGroup>
                        <FormGroup className="d-flex justify-content-center align-content-center">
                            <Button
                                color="primary"
                                onClick={this.handleOnSubmit}
                            >
                                Search
                            </Button>
                        </FormGroup>
                    </Col>
                </Row>
                {
                    this.state.isLoading ?
                        <Row>
                            <Col className="d-flex justify-content-center align-content-center">
                                <ReactLoading type="bars" />
                            </Col>
                        </Row> : get(this.state, 'data') && !!get(this.state, 'data.count')  && (
                        <div className="text-center">
                            <Row>
                                <Col>
                                    <AverageGauge value={get(this.state, 'data.average', 0)}/>
                                    <div className={styles.label}>Customer Satisfaction Meter</div>
                                </Col>
                            </Row>
                            {this.renderSentimentPercentages()}
                            <Row><Col className="my-5">{description}</Col></Row>
                        </div>
                    )
                }
                {get(this.state, 'data') && !get(this.state, 'data.count') && <h3 className="d-flex justify-content-center align-content-center mt-5">No tweets found with specified keyword</h3>
}
            </Container>
        );
    }
}

export default App;
