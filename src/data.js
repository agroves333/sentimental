export default [
    {
        entity: 'E1',
        sentiment: 0.0,
        magnitude: 0.5,
    },
    {
        entity: 'E2',
        sentiment: 1,
        magnitude: 0.5,
    },
    {
        entity: 'E3',
        sentiment: 0.3,
        magnitude: 0.5,
    },
    {
        entity: 'E4',
        sentiment: -0.5,
        magnitude: 0.5,
    },
    {
        entity: 'E5',
        sentiment: 0.1,
        magnitude: 0.5,
    },
    {
        entity: 'E6',
        sentiment: 1,
        magnitude: 0.5,
    },
]

export const description = 'Above you will find the average sentiment for each entity. Each average sentiment is indicated by the needle, whereas the three colored areas indicate positive, neutral and negative'