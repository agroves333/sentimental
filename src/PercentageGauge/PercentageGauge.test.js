import React from 'react';
import ReactDOM from 'react-dom';
import PercentageGauge from './PercentageGauge';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PercentageGauge />, div);
  ReactDOM.unmountComponentAtNode(div);
});
