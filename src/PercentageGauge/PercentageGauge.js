import React from 'react';
import PropTypes from 'prop-types';
import {Doughnut} from 'react-chartjs-2';
import classnames from 'classnames';
import styles from './PercentageGauge.module.css';

const PercentageGauge = ({value, color, label, total}) => {

    const newValue = (value * 100).toFixed(1);
    const data = {
        datasets: [{
            backgroundColor: [
                color,
                '#a5a5a5',
            ],
            data: [newValue, 100 - newValue],
        }]
    };

    return <div className={styles.wrapper}>
        <Doughnut className={styles.graph} data={data} />
        <strong className={styles.percent}>{newValue}%</strong>
        <div className={classnames(styles.label, 'pt-2')}>{label}</div>
        <div className={styles.total}># of Tweets: {total}</div>
    </div>
};

PercentageGauge.propTypes = {
    value: PropTypes.number,
    color: PropTypes.string,
    total: PropTypes.number,
};

PercentageGauge.defaultProps = {
    value: 0.5
};

export default PercentageGauge;
