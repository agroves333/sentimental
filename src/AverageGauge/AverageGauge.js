import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactSpeedometer from "react-d3-speedometer";
import styles from './AverageGauge.module.css';

class AverageGauge extends Component {
    
    render() {
        const value = (this.props.value + 1) * 50;
        return (
            <div className={styles.entityChart}>
                <ReactSpeedometer
                    maxValue={100}
                    value={Number(value.toFixed(2))}
                    needleColor="white"
                    startColor="#eb0100"
                    segments={3}
                    endColor="#0bd500"
                />
            </div>
        )
    }
}

AverageGauge.propTypes = {
    value: PropTypes.number
};

export default AverageGauge;
