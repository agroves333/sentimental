import React from 'react';
import ReactDOM from 'react-dom';
import AverageGauge from './AverageGauge';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AverageGauge />, div);
  ReactDOM.unmountComponentAtNode(div);
});
